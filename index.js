//console.log("hi");

/*
	JS DOM - Javascript Document Object Model

	For CSS, All html elements were considered as a box. This is our CSS Box Model. For Javascript, all HTML elements is considered as an object. This is what we call JS Document Object Model

	Since in JS, all html elements are objects and can be selected, we can manipulate and interactions to our web pages using javascript.


*/


//document is a keyword in JS which refers to the whole document. To the while html page.
console.log(document);

//store/select an element from our page to a variable.
//querySelector() is a method which can be used to select a specific element from our document. The querySelector() uses CSS-like notation/selector to select an element.
let firstNameLabel = document.querySelector("#label-first-name");

//stored/selected the element with the id label-first-name. we can then manipulate the element using JS.
console.log(firstNameLabel);


let lastNameLabel = document.querySelector("#label-last-name");
console.log(firstNameLabel);


//We can access properties from our elements, because JS considers html elements as Objects.
//innerHTML is a property of an element which considers all the children of the selected element as string. This includes other elements and text content.
console.log(firstNameLabel.innerHTML);

firstNameLabel.innerHTML = "I like New York City.";

lastNameLabel.innerHTML = "My Favorite Food is Adobo."

//User if-else statements to change the text of our label based on a condition.

let city = "Tokyo";
if(city === "New York"){
	firstNameLabel.innerHTML = `I like New York City.`
}
else{
	firstNameLabel.innerHTML = `I don't like New York. I like ${city} city.`
}


//Events

firstNameLabel.addEventListener('click',()=>{

	firstNameLabel.innerHTML = "I've been clicked. Send help!";

	firstNameLabel.style.color = "red";
	firstNameLabel.style.fontSize = "10vh";
})


lastNameLabel.addEventListener('click',()=>{

	lastNameLabel.style.color = "blue";
	lastNameLabel.style.fontSize = "5vh";

	if(lastNameLabel.style.color = "blue"){
		lastNameLabel.addEventListener('click',()=>{
			lastNameLabel.style.color = "black"
			lastNameLabel.style.fontSize = "16px"
		})
	}
})


let inputFirstName = document.querySelector("#txt-first-name");

console.log(inputFirstName.value);

//.value is property of mostly input elements. it contains the current value of the element.

/*inputFirstName.addEventListener('keyup',()=>{

	console.log(inputFirstName.value)
})*/

let fullNameDisplay = document.querySelector("#full-name-display")

console.log(fullNameDisplay)


let inputLastName= document.querySelector('#txt-last-name')
console.log(inputLastName)

/*
inputLastName.addEventListener('keyup',()=>{
	console.log(inputLastName.value)
})*/

/*inputlastName.addEventListener('keyup',()=>{

})*/

const showName = () => {
	console.log(inputFirstName.value)
	console.log(inputLastName.value)
	//console.log(fullNameDisplay.innerHTML)

	fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;
}

inputFirstName.addEventListener('keyup',showName);

inputLastName.addEventListener('keyup',showName);